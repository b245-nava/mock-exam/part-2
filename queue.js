let collection = [];

// Write the queue functions below.

function print() {
    // It will show the array
    return collection
}

function enqueue(element) {
    //In this funtion you are going to make an algo that will add an element to the array
    // Mimic the function of push method

    collection[collection.length] = element
    return collection
}

function dequeue() {
    // In here you are going to remove the last element in the array

    // Removes first element in array when invoked
    for(let i = 1; i < collection.length; i++){
        // Targets first element; collection[0] = collection[i]
        collection[i-1] = collection[i];
        //Decrementation of array length
        collection.length --;
        return collection

    // This function simply removes the first element from the array.
    }
}

function front() {
    // In here, you are going to remove the first element

    // Variable to contain first element
    let firstEl = "";

    /*for(let i = 0; i < collection.length; i++){
        collection[i-1] = collection[i]
        firstEl = collection[i]*/
    // Returns first element
    firstEl = collection[0]

        return firstEl
    // }
    // This function simply captures the first element's value, contains it in a variable, and returns the variable, but should not remove it from the original array.
    // If you return collection instead, Jane will still be included in the original array.
}

// starting from here, di na pwede gumamit ng .length property
function size() {
     // Number of elements

    let queueLength = 0;
    while(collection[queueLength] !== undefined){
        queueLength++;
        continue;
    }
    return queueLength;
}

function isEmpty() {
    //it will check whether the function is empty or not
    if(collection.length > 0){
        return false
    }
    return true
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};